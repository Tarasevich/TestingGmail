import pageObject.LoginPage;
import pageObject.MainPage;
import pageObject.PasswordPage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestGmail {

    public static LoginPage loginPage;
    public static PasswordPage passwordPage;
    public static MainPage mainPage;

    @BeforeClass
    public void startChrome(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\рома\\Desktop\\chromedriver.exe");
        loginPage = new LoginPage();
        passwordPage = new PasswordPage();
        mainPage = new MainPage();
        loginPage.initDriver();
    }

    @Test
    public void startGmail(){
        LoginPage.getDriver().get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
        LoginPage.getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        String currentURL = LoginPage.getDriver().getCurrentUrl();
        Assert.assertTrue(currentURL.contains("google.com/signin"));
    }

    @Test(dependsOnMethods = "startGmail")
    public void inputLogin(){
        loginPage.typeLogin();
        loginPage.nextBtn();
        boolean logo = LoginPage.getDriver().findElement(By.xpath("//*[@id=\"headingText\"]")).isDisplayed();
        Assert.assertTrue(logo);
    }

    @Test(dependsOnMethods = "inputLogin")
    public void inputPassword() throws InterruptedException {
        passwordPage.typePassword();
        TimeUnit.SECONDS.sleep(1);  //artificially added pauses for better demonstration of performing step-by-step actions
        passwordPage.submitBtn();
    }

    @Test(dependsOnMethods = "inputPassword")
    public void writeNewMessage(){
        mainPage.writeNewMessage();
        boolean logo = LoginPage.getDriver().findElement(By.id("link_vsm")).isDisplayed();
        Assert.assertTrue(logo);
    }

    @Test(dependsOnMethods = "writeNewMessage")
    public void checkInputMessages() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);    //artificially added pauses for better demonstration of performing step-by-step actions
        mainPage.openInputMessages();
        boolean newMessage = LoginPage.getDriver().findElement(By.xpath("//*[@id=\":2o\"]/div[5]")).isDisplayed();
        Assert.assertTrue(newMessage);
    }

    @Test(dependsOnMethods = "checkInputMessages")
    public void openNewMessage() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);   //artificially added pauses for better demonstration of performing step-by-step actions
        mainPage.checkNewMessage();
        TimeUnit.SECONDS.sleep(1);   //artificially added pauses for better demonstration of performing step-by-step actions
        String inputText = LoginPage.getDriver().findElement(By.id(":80")).getText();
        Assert.assertTrue(inputText.contains("Hello, World!"));
    }

    @AfterClass
    public void logoutGmail(){
        LoginPage.getDriver().quit();
    }
}

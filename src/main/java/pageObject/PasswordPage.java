package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class PasswordPage {

    By password = By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input");
    By submitButton = By.xpath("//*[@id=\"passwordNext\"]/div[2]");


    public PasswordPage typePassword(){
        LoginPage.getDriver().findElement(password).sendKeys("userTest");
        return this;
    }

    public MainPage submitBtn(){
        WebElement element = LoginPage.getDriver().findElement(submitButton);
        Actions actions = new Actions(LoginPage.getDriver());
        actions.moveToElement(element).click().perform();
        return new MainPage();
    }

}

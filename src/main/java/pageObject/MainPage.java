package pageObject;

import org.openqa.selenium.By;


public class MainPage {

    By writeMessageButton = By.xpath("//*[@id=\":3e\"]/div");
    By address = By.id(":8f");
    By textArea = By.id(":91");
    By sendMessageButton = By.id(":7n");
    By inputMsgButton = By.xpath("//*[@id=\":3m\"]/div");
    By newMessage = By.id(":51");



    public void writeNewMessage(){
        LoginPage.getDriver().findElement(writeMessageButton).click();
        LoginPage.getDriver().findElement(address).sendKeys("userTest032018@gmail.com");
        LoginPage.getDriver().findElement(textArea).sendKeys("Hello, World!");
        LoginPage.getDriver().findElement(sendMessageButton).click();
    }

    public void openInputMessages(){
        LoginPage.getDriver().findElement(inputMsgButton).click();
    }

    public void checkNewMessage(){
        LoginPage.getDriver().findElement(newMessage).click();

    }
}

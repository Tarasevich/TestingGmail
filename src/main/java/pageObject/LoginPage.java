package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LoginPage {

    By username = By.xpath("//*[@id=\"identifierId\"]");
    By nextButton = By.xpath("//*[@id=\"identifierNext\"]/div[2]");
    private static final String LOGIN = "userTest032018";

    private static WebDriver driver = new ChromeDriver();



     public static WebDriver getDriver(){
         if(driver == null){
             return new ChromeDriver();
         }else {
             return driver;
         }
     }

    public void initDriver(){
        getDriver();
        driver.manage().window().maximize();
    }

    public LoginPage typeLogin(){
        initDriver();
        driver.findElement(username).sendKeys(LOGIN);
        return this;
    }

    public PasswordPage nextBtn(){
        Actions actions = new Actions(driver);
        WebElement element = driver.findElement(nextButton);
        actions.moveToElement(element).click().perform();
        return new PasswordPage();
    }

}
